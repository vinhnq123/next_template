import Main from '@/components/web/Main'
import Navigation from '@/components/web/Navigation'

export default function Nav() {
  return (
    <>
      <Navigation />
      <Main />
    </>

  )
}
